import tensorflow as tf
import tensorlayer as tl
from tensorlayer.layers import *


def anvv6(image, reuse):
    tl.layers.set_name_reuse(reuse)
    with tf.variable_scope("anv_v6", reuse=reuse):
        with tf.variable_scope("color_up", reuse=reuse):
            x_in = InputLayer(image, name='input')
            x = Conv2d(x_in, n_filter=12, filter_size=(1, 1), strides=(1, 1), padding='SAME', name='ConvNdBackward1')
            slice_1, slice_2 = x[:, :, :, :6], x[:, :, :, 6:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward5')
        with tf.variable_scope("color_down", reuse=reuse):
            x = Conv2d(x, n_filter=6, filter_size=(1, 1), strides=(1, 1), padding='SAME', name='ConvNdBackward6')
            slice_1, slice_2 = x[:, :, :, :3], x[:, :, :, 3:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward10')
        with tf.variable_scope("conv1", reuse=reuse):
            x = Conv2d(x, n_filter=96, filter_size=(5, 5), strides=(1, 1), padding='SAME', name='ConvNdBackward11')
            slice_1, slice_2 = x[:, :, :, :48], x[:, :, :, 48:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward15')
            x1 = MaxPool2d(x, filter_size=(2, 2), strides=(2, 2), padding='SAME', name='MaxPool2dBackward16')
            x2 = MeanPool2d(x, filter_size=(2, 2), strides=(2, 2), padding='SAME', name='AvgPool2dBackward18')
            x12 = tl.layers.ElementwiseLayer([x1, x2], combine_fn=tf.add, name='AddBackward19')
            # ----------------------------------------------------------------------------------------------------------
        with tf.variable_scope("block1", reuse=reuse):
            x = Conv2d(x12, n_filter=96, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward20')
            slice_1, slice_2 = x[:, :, :, :48], x[:, :, :, 48:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward24')
            x = Conv2d(x, n_filter=96, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward25')
            slice_1, slice_2 = x[:, :, :, :48], x[:, :, :, 48:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward29')
            x = tl.layers.ElementwiseLayer([x12, x], combine_fn=tf.add, name='AddBackward31')
            # --------------------------------------------VERIFIED------------------------------------------------------
        with tf.variable_scope("group1"):
            x = Conv2d(x, n_filter=96, filter_size=(1, 1), strides=(1, 1), padding='SAME', name='ConvNdBackward32')
            slice_1, slice_2 = x[:, :, :, :48], x[:, :, :, 48:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward36')
            x = Conv2d(x, n_filter=192, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward37')
            slice_1, slice_2 = x[:, :, :, :96], x[:, :, :, 96:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward41')
            x1 = MaxPool2d(x, filter_size=(2, 2), strides=(2, 2), padding='SAME', name='MaxPool2dBackward42')
            x2 = MeanPool2d(x, filter_size=(2, 2), strides=(2, 2), padding='SAME', name='AvgPool2dBackward44')
            x12 = tl.layers.ElementwiseLayer([x1, x2], combine_fn=tf.add, name='AddBackward45')
            # -------------------------------------------VERIFIED-------------------------------------------------------
        with tf.variable_scope("block2"):
            x = Conv2d(x12, n_filter=192, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward46')
            slice_1, slice_2 = x[:, :, :, :96], x[:, :, :, 96:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward50')
            x = Conv2d(x, n_filter=192, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward51')
            slice_1, slice_2 = x[:, :, :, :96], x[:, :, :, 96:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward55')
            x12 = tl.layers.ElementwiseLayer([x12, x], combine_fn=tf.add, name='AddBackward57')
            # --------------------------------------------VERIFIED------------------------------------------------------
            x = Conv2d(x12, n_filter=192, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward58')
            slice_1, slice_2 = x[:, :, :, :96], x[:, :, :, 96:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward62')
            x = Conv2d(x, n_filter=192, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward63')
            slice_1, slice_2 = x[:, :, :, :96], x[:, :, :, 96:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward67')
            x12 = tl.layers.ElementwiseLayer([x12, x], combine_fn=tf.add, name='AddBackward69')
        with tf.variable_scope("group2"):
            # -----------------------------------------VERIFIED---------------------------------------------------------
            x = Conv2d(x12, n_filter=192, filter_size=(1, 1), strides=(1, 1), padding='SAME', name='ConvNdBackward70')
            slice_1, slice_2 = x[:, :, :, :96], x[:, :, :, 96:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward74')
            x = Conv2d(x, n_filter=384, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward75')
            slice_1, slice_2 = x[:, :, :, :192], x[:, :, :, 192:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward79')
            x1 = MaxPool2d(x, filter_size=(2, 2), strides=(2, 2), padding='SAME', name='MaxPool2dBackward80')
            x2 = MeanPool2d(x, filter_size=(2, 2), strides=(2, 2), padding='SAME', name='AvgPool2dBackward82')
            x12 = tl.layers.ElementwiseLayer([x1, x2], combine_fn=tf.add, name='AddBackward83')
        with tf.variable_scope("block3"):
            # -----------------------------------------------------------------------------------------------------------
            x = Conv2d(x12, n_filter=384, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward84')
            slice_1, slice_2 = x[:, :, :, :192], x[:, :, :, 192:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward88')
            x = Conv2d(x, n_filter=384, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward89')
            slice_1, slice_2 = x[:, :, :, :192], x[:, :, :, 192:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward93')
            x12 = tl.layers.ElementwiseLayer([x12, x], combine_fn=tf.add, name='AddBackward95')
            # -----------------------------------------------------------------------------------------------------------
            x = Conv2d(x12, n_filter=384, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward96')
            slice_1, slice_2 = x[:, :, :, :192], x[:, :, :, 192:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward100')
            x = Conv2d(x, n_filter=384, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward101')
            slice_1, slice_2 = x[:, :, :, :192], x[:, :, :, 192:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward105')
            x12 = tl.layers.ElementwiseLayer([x12, x], combine_fn=tf.add, name='AddBackward107')
            # --------------------------------------------VERIFIED------------------------------------------------------
            x = Conv2d(x12, n_filter=384, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward108')
            slice_1, slice_2 = x[:, :, :, :192], x[:, :, :, 192:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward112')
            x = Conv2d(x, n_filter=384, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward113')
            slice_1, slice_2 = x[:, :, :, :192], x[:, :, :, 192:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward117')
            x = tl.layers.ElementwiseLayer([x12, x], combine_fn=tf.add, name='AddBackward119')
        # -------------------------------------------------VERIFIED-----------------------------------------------------
        with tf.variable_scope("group3"):
            x = Conv2d(x, n_filter=384, filter_size=(1, 1), strides=(1, 1), padding='SAME', name='ConvNdBackward120')
            slice_1, slice_2 = x[:, :, :, :192], x[:, :, :, 192:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward124')
            x = Conv2d(x, n_filter=256, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward125')
            slice_1, slice_2 = x[:, :, :, :128], x[:, :, :, 128:]
            x12 = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward129')
        # -----------------------------------------------------------------------------------------------------------
        with tf.variable_scope("block4"):
            x = Conv2d(x12, n_filter=256, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward130')
            slice_1, slice_2 = x[:, :, :, :128], x[:, :, :, 128:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward134')
            x = Conv2d(x, n_filter=256, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward135')
            slice_1, slice_2 = x[:, :, :, :128], x[:, :, :, 128:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward139')
            x12 = tl.layers.ElementwiseLayer([x12, x], combine_fn=tf.add, name='AddBackward141')
            # -----------------------------------VERIFIED---------------------------------------------------------------
            x = Conv2d(x12, n_filter=256, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward142')
            slice_1, slice_2 = x[:, :, :, :128], x[:, :, :, 128:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward146')
            x = Conv2d(x, n_filter=256, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward147')
            slice_1, slice_2 = x[:, :, :, :128], x[:, :, :, 128:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward151')
            x12 = tl.layers.ElementwiseLayer([x12, x], combine_fn=tf.add, name='AddBackward153')
            # --------------------------------VERIFIED------------------------------------------------------------------
            x = Conv2d(x12, n_filter=256, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward154')
            slice_1, slice_2 = x[:, :, :, :128], x[:, :, :, 128:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward158')
            x = Conv2d(x, n_filter=256, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward159')
            slice_1, slice_2 = x[:, :, :, :128], x[:, :, :, 128:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward163')
            x12 = tl.layers.ElementwiseLayer([x12, x], combine_fn=tf.add, name='AddBackward165')
            # -----------------------------------VERIFIED---------------------------------------------------------------
            x = Conv2d(x12, n_filter=256, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward166')
            slice_1, slice_2 = x[:, :, :, :128], x[:, :, :, 128:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward170')
            x = Conv2d(x, n_filter=256, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward171')
            slice_1, slice_2 = x[:, :, :, :128], x[:, :, :, 128:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward175')
            x = tl.layers.ElementwiseLayer([x12, x], combine_fn=tf.add, name='AddBackward177')
        # ------------------------------------------VERIFIED------------------------------------------------------------
        with tf.variable_scope("group4"):
            x = Conv2d(x, n_filter=256, filter_size=(1, 1), strides=(1, 1), padding='SAME', name='ConvNdBackward178')
            slice_1, slice_2 = x[:, :, :, :128], x[:, :, :, 128:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward182')
            x = Conv2d(x, n_filter=256, filter_size=(3, 3), strides=(1, 1), padding='SAME', name='ConvNdBackward183')
            slice_1, slice_2 = x[:, :, :, :128], x[:, :, :, 128:]
            x = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='CmaxBackward187')
            x1 = MaxPool2d(x, filter_size=(2, 2), strides=(2, 2), padding='SAME', name='MaxPool2dBackward188')
            x2 = MeanPool2d(x, filter_size=(2, 2), strides=(2, 2), padding='SAME', name='AvgPool2dBackward190')
            x12 = tl.layers.ElementwiseLayer([x1, x2], combine_fn=tf.add, name='AddBackward191')
        # ------------------------------------------VERIFIED------------------------------------------------------------
        with tf.variable_scope("fc1"):
            x = TransposeLayer(x12, perm=[0, 3, 1, 2])
            x = FlattenLayer(x, name='flatten')
            x = DenseLayer(x, n_units=512, name='AddmmBackward192')
            slice_1, slice_2 = x[:, :256], x[:, 256:]
        x_out = tl.layers.ElementwiseLayer([slice_1, slice_2], combine_fn=tf.maximum, name='fc1')
    return x_out


if __name__ == '__main__':
    image = tf.placeholder(shape=(None, 128, 128, 3), dtype=tf.float32)
    k = anvv6(image, reuse=tf.AUTO_REUSE)
    a = 0
