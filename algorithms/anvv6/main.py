# coding: utf-8
import mxnet as mx
from mini_pipeline.mtcnn_detector import MtcnnDetector
import numpy as np
import cv2
import os
import time
from sklearn.metrics.pairwise import cosine_similarity

from mini_pipeline.zen_rescnn_cm import ZenResCNNCMdeploy
import torch
from torch.autograd import Variable


def prepare_img(img, target_size=128):
    yy = int((img.shape[0] - target_size) / 2)
    xx = int((img.shape[1] - target_size) / 2)
    resized_img = img[yy: yy + target_size, xx: xx + target_size]

    result = np.zeros((1, 3, target_size, target_size), dtype=np.float32)

    # RGB data within [0,1]
    result[0, 2, :, :] = resized_img[:, :, 0]
    result[0, 1, :, :] = resized_img[:, :, 1]
    result[0, 0, :, :] = resized_img[:, :, 2]
    result = result / 255.0

    result = torch.from_numpy(result)
    result = Variable(result).cuda()

    return result


def load_weight(model, state_dict):
    '''
        load weight from state_dict to model, skip those with invalid shape
    :param model:
    :param state_dict:
    :return:
    '''
    for name, param in model.named_parameters():
        if not name in state_dict:
            continue
        if param.size() == state_dict[name].size():
            print('==> loading weight from ', name)
            param.data.copy_(state_dict[name])


# load detector
detector = MtcnnDetector(model_folder='model', ctx=mx.gpu(0), num_worker=1, accurate_landmark=True)

# load feature extractor
checkpoint_file = 'zencnn_lm.pth'
model = ZenResCNNCMdeploy()
model.eval()
model.cuda()

# load state_dict
sd = torch.load(checkpoint_file)
sd = sd['state_dict']
sd = {k.partition('module.')[2]: v for k, v in sd.items()}
load_weight(model, sd)
print('-> FR model loaded')

# detect and align images
# enhanced
# img1 = cv2.imread('/media/soumya/HDD_1/testing_subset/to_test/Ours/001/001_cam4_1.png')
img1 = cv2.imread('/media/soumya/HDD_1/Face_Datasets/scface/cropped/probe/001/001_cam4_1.png')
results = detector.detect_face(img1)

if results is not None:
    total_boxes, points = results
    # extract aligned face chips
    chips = detector.extract_image_chips(img1, points, 144, 0.37)
    chip1 = chips[0]

img2 = cv2.imread('/media/soumya/HDD_1/Face_Datasets/scface/cropped/gallery/001/001_frontal.png')
results = detector.detect_face(img2)
if results is not None:
    total_boxes, points = results
    chips = detector.extract_image_chips(img2, points, 144, 0.37)
    chip2 = chips[0]

if chip1 is None or chip2 is None:
    print('can not detect face')
    exit()

in_data1 = prepare_img(chip1, target_size=128)
in_data2 = prepare_img(chip2, target_size=128)

f1 = model(in_data1)
f2 = model(in_data2)

f1 = f1.cpu().data.numpy()
f2 = f2.cpu().data.numpy()

score = cosine_similarity(f1, f2)
print('similar score is ', score)

# cv2.imshow('img1', chip1)
# cv2.imshow('img2', chip2)
# cv2.waitKey(0)
