import tensorflow as tf
from core.layers import conv2d, conv3d
from core.layers import relu
from core.layers import conv2d_dilation, conv3d_dilation
from core.layers import dense
from core.layers import upsampling_nn
from utils.spatial_transform import spatial_transformer_network as stn
import cv2
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
import math
import time
import tqdm
import os
import argparse
import copy
import sys


# Alignment Encoder
class AlignmentEncoder(object):
    def __init__(self):
        self.mean = [0.485, 0.456, 0.406]
        self.std = [0.229, 0.224, 0.225]

    def forward(self, input_):
        with tf.variable_scope('alignment_encoder', reuse=tf.AUTO_REUSE):
            input_ = (input_ - self.mean) / self.std
            x = conv2d(input_, filters=64, kernel_size=5, strides=2, initializer='init_he_out', name='conv_1')
            x = relu(x)
            x = conv2d(x, filters=64, kernel_size=3, strides=1, initializer='init_he_out', name='conv_2')
            x = relu(x)
            x = conv2d(x, filters=128, kernel_size=3, strides=2, initializer='init_he_out', name='conv_3')
            x = relu(x)
            x = conv2d(x, filters=128, kernel_size=3, strides=1, initializer='init_he_out', name='conv_4')
            x = relu(x)
            x = conv2d(x, filters=256, kernel_size=3, strides=2, initializer='init_he_out', name='conv_5')
            x = relu(x)
            x = conv2d(x, filters=256, kernel_size=3, strides=1, initializer='init_he_out', name='conv_6')
            x = relu(x)
            x = conv2d(x, filters=256, kernel_size=3, strides=1, initializer='init_he_out', name='conv_7')
            x = relu(x)
        return x


# Alignment Regressor
class AlignmentRegressor(object):
    def __init__(self):
        pass

    def forward(self, input_):
        with tf.variable_scope('alignment_regressor', reuse=tf.AUTO_REUSE):
            x = conv2d(input_, filters=512, kernel_size=3, strides=2, initializer='init_he_out', name='conv_1')
            x = relu(x)
            x = conv2d(x, filters=512, kernel_size=3, strides=1, initializer='init_he_out', name='conv_2')
            x = relu(x)
            x = conv2d(x, filters=512, kernel_size=3, strides=1, initializer='init_he_out', name='conv_3')
            x = relu(x)
            x = conv2d(x, filters=512, kernel_size=3, strides=2, initializer='init_he_out', name='conv_4')
            x = relu(x)
            x = conv2d(x, filters=512, kernel_size=3, strides=1, initializer='init_he_out', name='conv_5')
            x = relu(x)
            x = conv2d(x, filters=512, kernel_size=3, strides=1, initializer='init_he_out', name='conv_6')
            x = relu(x)
            x = tf.reduce_mean(x, [1, 2], keep_dims=True)
            x = tf.reshape(x, (-1, x.shape[-1].value))
            x = dense(x, 6)
            x = tf.reshape(x, (-1, 2, 3))
        return x


# Encoder (Copy network)
class CopyEncoder(object):
    def __init__(self):
        self.mean = [0.485, 0.456, 0.406]
        self.std = [0.229, 0.224, 0.225]

    def forward(self, input_):
        with tf.variable_scope('copy_encoder', reuse=tf.AUTO_REUSE):
            input_ = (input_ - self.mean) / self.std
            x = conv2d(input_, filters=64, kernel_size=5, strides=2, initializer='init_he_out', name='conv_1')
            x = relu(x)
            x = conv2d(x, filters=64, kernel_size=3, strides=1, initializer='init_he_out', name='conv_2')
            x = relu(x)
            x = conv2d(x, filters=128, kernel_size=3, strides=2, initializer='init_he_out', name='conv_3')
            x = relu(x)
            x = conv2d(x, filters=128, kernel_size=3, strides=1, initializer='init_he_out', name='conv_4')
            x = relu(x)
            x = conv2d(x, filters=128, kernel_size=3, strides=1, initializer='init_he_out', name='conv_5')
        return x


# Decoder (Paste network)
class PasteDecoder(object):
    def __init__(self):
        self.mean = [0.485, 0.456, 0.406]
        self.std = [0.229, 0.224, 0.225]

    def forward(self, input_):
        with tf.variable_scope('paste_decoder', reuse=tf.AUTO_REUSE):
            x = conv2d(input_, filters=257, kernel_size=3, strides=1, initializer='init_he_out', name='conv_1')
            x = relu(x)
            x = conv2d(x, filters=257, kernel_size=3, strides=1, initializer='init_he_out', name='conv_2')
            x = relu(x)
            x = conv2d(x, filters=257, kernel_size=3, strides=1, initializer='init_he_out', name='conv_3')
            x = relu(x)
            # dilated convolution blocks
            with tf.variable_scope('dilated'):
                x = conv2d_dilation(x, filters=257, dilation=2, kernel_size=3, initializer='init_he_out', name='conv_1')
                x = relu(x)
                x = conv2d_dilation(x, filters=257, dilation=4, kernel_size=3, initializer='init_he_out', name='conv_2')
                x = relu(x)
                x = conv2d_dilation(x, filters=257, dilation=8, kernel_size=3, initializer='init_he_out', name='conv_3')
                x = relu(x)
                x = conv2d_dilation(x, filters=257, dilation=16, kernel_size=3, initializer='init_he_out',
                                    name='conv_4')
                x = relu(x)
            # normal convolution
            x = conv2d(x, filters=257, kernel_size=3, strides=1, initializer='init_he_out', name='conv_4')
            x = relu(x)
            x = conv2d(x, filters=128, kernel_size=3, strides=1, initializer='init_he_out', name='conv_5')
            x = relu(x)
            x = conv2d(x, filters=128, kernel_size=3, strides=1, initializer='init_he_out', name='conv_6')
            x = relu(x)
            x = upsampling_nn(x, factor=2, interpolation='nearest')
            x = conv2d(x, filters=64, kernel_size=3, strides=1, initializer='init_he_out', name='conv_7')
            x = relu(x)
            x = conv2d(x, filters=64, kernel_size=3, strides=1, initializer='init_he_out', name='conv_8')
            x = relu(x)
            x = upsampling_nn(x, factor=2, interpolation='nearest')
            x = conv2d(x, filters=3, kernel_size=5, strides=1, initializer='init_he_out', name='conv_9')
            x = tf.multiply(x, tf.convert_to_tensor(self.std)) + tf.convert_to_tensor(self.mean)
        return x


# Context Matching Module
class ContextMatchingModule(object):
    def __init__(self):
        pass

    def masked_softmax(self, vec, axis):
        max_vec = tf.reduce_max(vec, axis=axis, keepdims=True)
        exps = tf.exp(vec - max_vec)
        sums = tf.reduce_sum(exps, axis=axis, keepdims=True)
        zeros = (sums < 1e-4)
        sums += tf.cast(zeros, tf.float32)
        return exps / sums

    def forward(self, values):
        B, T, H, W, C = values.shape
        # t_feat: target feature
        t_feat = values[:, 0, :, :, :]
        # r_feats: reference features
        r_feats = values[:, 1:, :, :, :]
        _, T, H, W, Cv = r_feats.shape
        gs_, vmap_ = [], []
        for r in range(T):
            gs = tf.reduce_sum(tf.reduce_sum(tf.reduce_sum((t_feat * r_feats[:, r, :, :, :]), axis=-1), axis=-1),
                               axis=-1)
            gs = gs / tf.cast(C, tf.float32)
            gs = tf.ones_like(t_feat) * tf.reshape(gs, (-1, 1, 1, 1))
            gs_.append(gs)
        gss = tf.stack(gs_, axis=1)
        # weighted pixelwise masked softmax
        c_mask = self.masked_softmax(gss, axis=1)
        c_out = tf.reduce_sum(r_feats * c_mask, axis=1)
        # c_mask
        c_mask = tf.reduce_sum(c_mask, axis=1)
        c_mask = 1. - (tf.reduce_mean(c_mask, axis=-1, keepdims=True))
        return tf.concat([t_feat, c_out, c_mask], axis=-1), c_mask