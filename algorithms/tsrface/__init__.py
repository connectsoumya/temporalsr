import algorithms
from algorithms.tsrface.dataloader import multipie_dataloader, multipie_path
from algorithms.tsrface.model_copypaste_net import AlignmentEncoder, AlignmentRegressor, CopyEncoder, PasteDecoder, \
    ContextMatchingModule
