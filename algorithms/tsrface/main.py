from algorithms.tsrface import multipie_dataloader, multipie_path
from algorithms.tsrface import AlignmentRegressor, AlignmentEncoder, CopyEncoder, PasteDecoder, ContextMatchingModule
from utils.spatial_transform import spatial_transformer_network as stn
import tensorflow as tf
from core.metrics import cosine_distance
from algorithms.anvv6.anvv6 import anvv6
from algorithms.vgg19.vgg19 import vgg19
import tensorlayer as tl
import cv2
import os
import numpy as np

config = tf.ConfigProto(allow_soft_placement=True)
config.gpu_options.allow_growth = True

optimizer = tf.train.AdamOptimizer(1e-4)
epochs = 1000
frames = 5
data_batch_tensor = tf.placeholder(dtype=tf.float32, shape=(None, frames, 128, 128, 3))
gt_batch_tensor = tf.placeholder(dtype=tf.float32, shape=(None, 128, 128, 3))
alenc = AlignmentEncoder()
aldec = AlignmentRegressor()
cpenc = CopyEncoder()
cmmodule = ContextMatchingModule()
pdec = PasteDecoder()
aligned = []
alencoded = []
cpencoded = []
for i in range(frames):
    alencoded.append(alenc.forward(data_batch_tensor[:, i, :, :, :]))
frame_nums = list(range(frames))
for i in frame_nums:
    ref_frames = frame_nums.copy()
    ref_frames.remove(i)
    frame = alencoded[i]
    for j in ref_frames:
        ref = alencoded[j]
        input_to_decoder = tf.concat([frame, ref], axis=-1)
        decoded = aldec.forward(input_to_decoder)
        aligned_tensor = stn(data_batch_tensor[:, i, :, :, :], decoded, data_batch_tensor.shape[2:-1].dims)
        cpencoded.append(cpenc.forward(aligned_tensor))
cmmod_input_tensor = tf.stack(cpencoded, axis=1)
pdec_tensor, cmask = cmmodule.forward(cmmod_input_tensor)
pred_face = pdec.forward(pdec_tensor)
################## Loss Function #######################
# Initialize Loss fn networks
net_vgg = vgg19(gt_batch_tensor, tf.AUTO_REUSE)
gt_vgg_feature = vgg19(gt_batch_tensor, True).outputs
predicted_vgg_feature = vgg19(pred_face, True).outputs
net_anvv6 = anvv6(gt_batch_tensor, tf.AUTO_REUSE)
gt_facial_feature = anvv6(gt_batch_tensor, True).outputs
predicted_facial_feature = anvv6(pred_face, True).outputs
facial_pixelwise_loss = tf.reduce_mean(tf.abs(pred_face - gt_batch_tensor))
facial_vgg_loss = tf.reduce_mean(tf.abs(predicted_vgg_feature - gt_vgg_feature))
facial_feature_loss = cosine_distance(predicted_facial_feature, gt_facial_feature)

total_loss = facial_pixelwise_loss*0.01 + facial_vgg_loss + 1 * facial_feature_loss
loss = optimizer.minimize(total_loss)

with tf.Session(config=config) as sess:
    sess.run(tf.global_variables_initializer())
    # # ===== LOAD ANV_V6 ===== # #
    params = np.load('../anvv6/model/anv_v6.weights.npy', encoding='latin1', allow_pickle=True)
    tl.files.assign_params(sess, params, net_anvv6)
    # # ===== LOAD VGG19 ===== # #
    if not os.path.isfile('../vgg19/model/vgg19.npy'):
        print('Please download vgg19.npz from : https://github.com/machrisaa/tensorflow-vgg')
        exit()
    npz = np.load('../vgg19/model/vgg19.npy', encoding='latin1', allow_pickle=True).item()
    params = []
    for val in sorted(npz.items()):
        W = np.asarray(val[1][0])
        b = np.asarray(val[1][1])
        print("  Loading %s: %s, %s" % (val[0], W.shape, b.shape))
        params.extend([W, b])
    tl.files.assign_params(sess, params[:30], net_vgg)
    # Train
    for ep in range(epochs):
        for databatch, gt_batch, id_list in multipie_dataloader(multipie_path, 8):
            _, difference, image = sess.run([loss, total_loss, pred_face], feed_dict={data_batch_tensor: databatch,
                                                                                gt_batch_tensor: gt_batch})
            print(ep, difference)
            image = ((image - np.min(image)) / (np.max(image) - np.min(image)))
            image = (image * 255).astype(np.uint8)
            # image = (np.clip(image[0, :, :, :], 0, 1) * 255).astype(np.uint8)
            cv2.imwrite('test.png', image[0, :, :, :])
