#! /usr/bin/python
# -*- coding: utf8 -*-
import time
from algorithms.srgan.model import *
from algorithms.srgan.utils import *
from algorithms.srgan.config import config


def evaluate():
    ## create folders to save result images
    save_dir = "../../data/srgan/samples/{}".format(tl.global_flag['mode'])
    tl.files.exists_or_mkdir(save_dir)
    # checkpoint_dir =
    valid_lr_img_list = sorted(tl.files.load_file_list(path=config.VALID.lr_img_path, regx='.*.png', printable=False))
    valid_lr_imgs = tl.vis.read_images(valid_lr_img_list, path=config.VALID.lr_img_path, n_threads=2)
    t_image = tf.placeholder('float32', [None, None, None, 3], name='input_image')
    net_g = SRGAN_g(t_image, is_train=False, reuse=False)
    ###========================== RESTORE G =============================###
    gpu_config = tf.ConfigProto(allow_soft_placement=True)
    gpu_config.gpu_options.allow_growth = True
    sess = tf.Session(config=gpu_config)
    tl.layers.initialize_global_variables(sess)
    tl.files.load_and_assign_npz(sess=sess, name='../../data/srgan_model/g_srgan.npz', network=net_g)
    ###======================= EVALUATION =============================###
    for imid in range(len(valid_lr_img_list)):
        imname = valid_lr_img_list[imid]
        valid_lr_img = valid_lr_imgs[imid]
        # valid_hr_img = valid_hr_imgs[imid]
        # valid_lr_img = get_imgs_fn('test.png', 'data2017/')  # if you want to test your own image
        valid_lr_img = (valid_lr_img / 127.5) - 1  # rescale to ［－1, 1]
        # print(valid_lr_img.min(), valid_lr_img.max())
        size = valid_lr_img.shape
        start_time = time.time()
        out = sess.run(net_g.outputs, {t_image: [valid_lr_img]})
        print("took: %4.4fs" % (time.time() - start_time))
        print("LR size: %s /  generated HR size: %s" % (size, out.shape))
        img = (out[0] + 1) * 255
        tl.vis.save_image(img.astype(np.uint8), save_dir + '/' + imname)


def super_resolve(image):
    pass


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', type=str, default='evaluate', help='srgan, evaluate')
    args = parser.parse_args()
    tl.global_flag['mode'] = args.mode
    if tl.global_flag['mode'] == 'evaluate':
        evaluate()
    else:
        raise Exception("Unknow --mode")
