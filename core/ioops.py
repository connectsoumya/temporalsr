import tensorflow as tf
import os
import logging
import logging.config
from tensorflow.python.tools import inspect_checkpoint
import errno
from PIL import Image
import cv2

my_path = os.path.abspath(os.path.dirname(__file__))
log_file_path = os.path.join(my_path, '../logging.ini')
logging.config.fileConfig(log_file_path)


def save(sess, filepath='../tmp/tfmodel.mdl', global_step=None):
    """
    Save a TensorFlow model.
    :param sess:
    :param filepath:
    :param global_step:
    :return:
    """
    saver = tf.train.Saver()
    saver.save(sess, filepath, global_step=global_step)
    logging.info('Model saved at ' + filepath)


def load(sess, filepath):
    """
    Load/Restore a TensorFlow model.
    :param sess: The session
    :param filepath:
    :return:
    """
    init_op = tf.global_variables_initializer()
    sess.run(init_op)
    saver = tf.train.Saver()
    inspect_checkpoint.print_tensors_in_checkpoint_file(filepath, tensor_name=None, all_tensors=False)
    saver.restore(sess, filepath)
    logging.info('Model restored from ' + filepath)
    return sess


def save_image_rgb(image, filename):
    """
    Save a numpy array to image.
    :param image: The numpy array
    :param filename:
    :param ext:
    :return:
    """
    try:
        os.makedirs(os.path.dirname(filename))
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(os.path.dirname(filename)):
            pass
        else:
            raise
    try:
        im = Image.fromarray(image)
    except TypeError:
        im = Image.fromarray(image, 'RGB')
    im.save(filename)


def save_image_bgr(image, filename):
    """
    Save a numpy array to image.
    :param image: The numpy array
    :param filename:
    :param ext:
    :return:
    """
    try:
        os.makedirs(os.path.dirname(filename))
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(os.path.dirname(filename)):
            pass
        else:
            raise
    cv2.imwrite(filename, image)
