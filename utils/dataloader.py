import cv2
import numpy as np
import glob
import os


def multipie_dataloader(path='/media/soumya/HDD_2/MultiPIE_crop/data', batch_size=8):
    sessions = ['session01', 'session02', 'session03', 'session04']
    ground_truth = '???_??_??_051_??.png'
    data_batch = []
    gt_batch = []
    id_list = []
    for session in sessions:
        session_path = os.path.join(path, session, 'multiview')
        identities = os.listdir(session_path)
        for id_ in identities:
            try:
                gt_path = glob.glob(os.path.join(session_path, id_, '01/*', ground_truth))[0]
            except IndexError:
                continue
            training_images_path = glob.glob(os.path.join(session_path, id_, '01/*', '*.png'))
            if len(training_images_path) < 6:
                continue
            training_images_path.remove(gt_path)
            training_images = [cv2.imread(img) for img in training_images_path]
            training_images = np.array(training_images)
            gt = cv2.imread(gt_path)
            data_batch.append(training_images)
            gt_batch.append(gt)
            id_list.append(id_)
            if len(data_batch) == batch_size:
                data_batch = np.array(data_batch)
                gt_batch = np.array(gt_batch)
                yield data_batch, gt_batch, id_list
                data_batch = []
                gt_batch = []
                id_list = []
